# ICA

masterブランチは現在の本番環境に反映されているデータになります。
developブランチは本番にはアップしていない、作業中のデータになります。  
  
## お知らせ（及びdevelopブランチ内の制作実績）の更新
/dist/js/topics.jsonが更新個所になります。波括弧から波括弧閉じの次のカンマまで上にコピペいただき、内容を追加してください。
（制作実績の更新は同ディレクトリ内のworks.jsonになります）


## サイトの更新前の環境導入
上記個所以外に追加の更新が必要な場合、作業環境の導入が必要です。
npmが必要になりますので、こちらを参考にしてください。  
https://qiita.com/sansaisoba/items/242a8ba95bf70ba179d3

インストールが終わりましたら、コンソールでこちらのコマンドを実行してください。

`
yarn
`

コンソールにDone in ****s. といった表示がされれば開発環境は導入完了です。

## 構成ファイルの解説
* /dist: 本番環境にアップすべきファイルはこちらに入ります。この中身のみ本番と同期します。  
* /node_modules: 開発環境で使うファイルが入ります。原則編集しません。  
* /src: 開発データが入っています。HTMLの元のソースコードはこちらです。  
* それ以外: 開発環境用のデータなので原則編集しません。  

### /src内
* /src/css/: スタイルシートが入っています。  
* /src/js/
  * components/: 各ページのソースコードはこちらに入ります。
  * lib/: そのほかのJS（背景の動き）はこちらに入ります。
* app.tsx: サイト全体のソースコードはこちらになります。
* ga.tsx: googleアナリティクス用のソースコードです。

## 更新方法
### 更新内容の反映
以下コマンドを実行してください。  
`
yarn build
`
ソースコード内にエラーが無ければ、更新済みのデータが/dist内に入ります。こちらをサーバーにアップし、内容をご確認ください。
テストアップする際は、index.html及びbundle.jsを別名にすれば大丈夫かと思います。


### ページを増やす場合
1. /src/js/components/内、Company.tsxをコピーし、任意の名前にリネームしてください。
2. ファイル内の"company"、"Company"の文字をすべてリネーム後の名前に置換します。大文字小文字混在しているので注意してください。
3. 

/src/js/app.tsx

```jsx
import err_404 from "./components/404";
import About from "./components/About";
import Company from "./components/Company";
import Contact from "./components/Contact";
import Footer from "./components/Footer";
import Gallery from "./components/Gallery";
import Header from "./components/Header";
import Home from "./components/Home";
import Pager from "./components/Pager";
import Policy from "./components/Policy";
import Thanks from "./components/Thanks";
import Topics from "./components/Topics";
import Works from "./components/Works";
// ここに追加
import "./lib/three_bg";
```

```jsx
            <PoseGroup>
              <RouteContainer key={props.location.pathname} className="animate">
                <Switch location={props.location}>
                  <Route exact path={`/`} component={Home} key="home" />
                  <Route path={`/about`} component={About} key="about" />
                  <Route path={`/company`} component={Company} key="company" />
                  <Route path={`/gallery`} component={Gallery} key="gallery" />
                  <Route path={`/works`} component={Works} key="Works" />
                  <Route path={`/topics`} component={Topics} key="topics" />
                  <Route path={`/contact`} component={Contact} key="contact" history={props.history} />
                  <Route path={`/thanks`} component={Thanks} key="thanks" />
                  <Route path={`/policy`} component={Policy} key="policy" />
                  // ここに追加
                  <Route component={err_404} />
                </Switch>
              </RouteContainer>
            </PoseGroup>
```

上記2か所のタグをcompanyのものからコピペしてください。これでつなぎこみは完了です。

```html
<div className="inline">~~~</div>
```

内が編集可能領域になります。