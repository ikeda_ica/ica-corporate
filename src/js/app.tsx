import * as React from "react";
import * as ReactDOM from "react-dom";
import * as ReactGA from "react-ga";
import posed, { PoseGroup } from "react-pose";
import { withRouter, BrowserRouter as Router, Route, Switch } from "react-router-dom";
import {
  CSSTransition,
  TransitionGroup,
} from "react-transition-group";
import err_404 from "./components/404";
import About from "./components/About";
import Company from "./components/Company";
import Contact from "./components/Contact";
import Footer from "./components/Footer";
import Gallery from "./components/Gallery";
import Header from "./components/Header";
import Home from "./components/Home";
import Pager from "./components/Pager";
import Policy from "./components/Policy";
import Thanks from "./components/Thanks";
import Topics from "./components/Topics";
import "./lib/three_bg";

const ga_id: string | undefined = process.env.GA_ID;

if(ga_id){
  ReactGA.initialize(ga_id);
}

import "../css/style.scss";

const RouteContainer = posed.div({
  enter: { opacity: 1, delay: 300, beforeChildren: true },
  exit: { opacity: 0, transition: { duration: 200 } }
});

const App = withRouter((props: any) => {
  const [active, setActive] = React.useState("");
  const [isIndex, setIsIndex] = React.useState(true);
  const [darkmode, setDarkmode] = React.useState(false);

  React.useEffect(() => {
    setActive(props.location.pathname)
    if(props.location.pathname === "/"){
      setIsIndex(true);
    }else{
      setIsIndex(false);
    }
    if(props.location.pathname === "/contact" || props.location.pathname === "/thanks") {
      setDarkmode(true);
    }else{
      setDarkmode(false);
    }
  });

  return(
    <div className={ darkmode ? "wrapper dark" : "wrapper"}>
      <Header location={props.location} active={active} darkmode={darkmode} />
          <div className="contents_wrapper">
            <PoseGroup>
              <RouteContainer key={props.location.pathname} className="animate">
                <Switch location={props.location}>
                  <Route exact path={`/`} component={Home} key="home" />
                  <Route path={`/about`} component={About} key="about" />
                  <Route path={`/company`} component={Company} key="company" />
                  <Route path={`/gallery`} component={Gallery} key="gallery" />
                  <Route path={`/topics`} component={Topics} key="topics" />
                  <Route path={`/contact`} component={Contact} key="contact" history={props.history} />
                  <Route path={`/thanks`} component={Thanks} key="thanks" />
                  <Route path={`/policy`} component={Policy} key="policy" />
                  <Route component={err_404} />
                </Switch>
              </RouteContainer>
            </PoseGroup>
                <CSSTransition
                  in={isIndex}
                  timeout={300}
                >
                  <div className="canvas_wrapper" id="render" />
                </CSSTransition>            
            <Pager active={active} match={props.match} />
          </div>
      <Footer />
    </div>
  );
})

const Index = () => {
  return (
    <Router>
      <App />
    </Router>
  );
};

ReactDOM.render(<Index />, document.getElementById("root"));
