import * as React from "react";
import * as ReactDOM from "react-dom";
import * as ReactGA from "react-ga";
import { withRouter } from "react-router-dom";

const GA = (props: any) => {
  React.useEffect(() => {
    ReactGA.pageview(window.location.pathname + window.location.search);      
  }, [props.location])

  return(
    <>
    </>
  );
}

export default withRouter(GA);