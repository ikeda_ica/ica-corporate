import * as THREE from "three"

let camera: THREE.PerspectiveCamera, scene: THREE.Scene, renderer: THREE.WebGLRenderer, renderElm: HTMLElement | null
let step: number = 0

const particleCount: number = 1000
const lifetime: number = 500

const particles: number = 1000;
const loop: number = particles * 3

const texture: string = "../assets/img/t_tex.png"

function getRandomInt(min: number, max: number) {
  return Math.floor(Math.random() * (max - min)) + min
}

function getRadian(deg: number) {
    return deg * Math.PI / 180
}

// function getDegree(rad: number) {
//     return rad * 180 / Math.PI
// }

function hsvToRgb(h: number, s: number, v: number) {
  const c = s * v
  const _h = h / 60
  const x = c * ( 1 - Math.abs(_h % 2 - 1))

  let r, g, b
  if(0 <= _h && _h < 1){
    [r, g, b] = [c, x, 0]
  }
  if(1 <= _h && _h < 2){
    [r, g, b] = [x, c, 0]
  }
  if(2 <= _h && _h < 3){
    [r, g, b] = [0, c, x]
  }
  if(3 <= _h && _h < 4){
    [r, g, b] = [0, x, c]
  }
  if(4 <= _h && _h < 5){
    [r, g, b] = [x, 0, c]
  }
  if(5 <= _h && _h < 6){
    [r, g, b] = [c, 0, x]
  }

  const m = v - c;
  if(r !== undefined && b !== undefined && g !== undefined){
    [r, g, b] = [r + m, g + m, b + m]
  }

  return {
    r: r,
    g: g,
    b: b
  }
}

const mouse = new THREE.Vector2()
function handleMouseMove(ev: any) {
  const elm= ev.currentTarget
  const x = ev.clientX - elm.offsetLeft
  const y = ev.clientY - elm.offsetTop

  const w = elm.offsetWidth
  const h = elm.offsetHeight

  // -1〜+1の範囲で現在のマウス座標を登録する
  mouse.x = ( x / w ) * 2 - 1
  mouse.y = -( y / h ) * 2 + 1
}

function init(){
  // camera
  camera = new THREE.PerspectiveCamera( 70, window.innerWidth / window.innerHeight, 1, 10000 )
  camera.position.x = 0
  camera.position.y = 0
  camera.position.z = 100
  camera.lookAt(0, 0, 0)
  
  // render settings
  renderer = new THREE.WebGLRenderer()
  renderer.setSize(window.innerWidth, window.innerHeight)
  renderer.setClearColor(0xffffff, 1.0)
	scene = new THREE.Scene()

  // create scene to canvas
  renderElm = document.getElementById("render")
  if(renderElm !== null){
    renderElm.appendChild(renderer.domElement)
    window.addEventListener("resize", resize)

    const loader = new THREE.TextureLoader()
    loader.load(texture, (res: any) => {
      drawParticle(res)
      requestAnimationFrame(tick)
    }) 
  }
}

let pGeometry: THREE.BufferGeometry
let pMaterial: THREE.PointsMaterial

let degrees: Array<number> = [];
let colors: Array<number> = [];
let ranges: Array<number> = [];
interface IPositions {
  x: number
  y: number
  z: number
}
let positions: Array<IPositions> = [];
let sizes: Array<number> = [];

function drawParticle(texture: THREE.Texture) {
  pGeometry = new THREE.BufferGeometry();
  for (let i = 0; i < particles; i++) {
    const deg = getRandomInt(0, 360);
    degrees.push(deg);

    let color = new THREE.Color();
    color.setHSL( i / particles, 1, 0.5 );
    colors.push( color.r, color.b, color.g )
    ranges.push(getRandomInt(40, 50));
    positions.push({
      x: getRandomInt(40, 50),
      y: getRandomInt(40, 50),
      z: getRandomInt(-10, 10)
    })
    sizes.push( 20 );
  }  

  pGeometry.addAttribute( "degree", new THREE.Float32BufferAttribute( degrees, 1 ));
  pGeometry.addAttribute( "range", new THREE.Float32BufferAttribute( ranges, 1 ));
  pGeometry.addAttribute( "color", new THREE.Float32BufferAttribute( colors, 3 ));
  pGeometry.addAttribute( "position", new THREE.BufferAttribute( new Float32Array( particles * 3 ), 3 ));
  pGeometry.addAttribute( "size", new THREE.Float32BufferAttribute( sizes, 1 ).setDynamic( true ));

  const pMaterial = new THREE.PointsMaterial({
    map: texture,
    size: 1,
    transparent: true,    
    depthTest: false,
    vertexColors: THREE.VertexColors,
  });

  const points = new THREE.Points(pGeometry, pMaterial)
  scene.add(points)
}

function updateParticle() {
  const _positions: any = pGeometry.attributes.position.array;
  const _degrees: any = pGeometry.attributes.degree.array;
  const _colors: any = pGeometry.attributes.color.array;
  const _ranges: any = pGeometry.attributes.range.array;

  let r = 0, g = 0, b = 0, index = 0;
  for ( let i = 0; i < particles; i++ ) {
    _degrees[i]++;
    pGeometry.attributes.position.setX( i , positions[i].x * Math.cos(getRadian(_degrees[i])));
    pGeometry.attributes.position.setY( i , positions[i].y * Math.sin(getRadian(_degrees[i])));
    const rgb = hsvToRgb(Math.abs(Math.sin(getRadian(step + i) / particles * 10)) * 360, 1, .9);
    _colors[ index ++ ] = rgb.r;
    _colors[ index ++ ] = rgb.g;
    _colors[ index ++ ] = rgb.b;
    

    if(i === 0){
      // console.log(_colors[0], _colors[1], _colors[2])
    }
  }
}

function updateCamera() {
  camera.position.x = -20 * mouse.x
  camera.position.y = -20 * mouse.y
  camera.lookAt(0, 0, 0)
}

const raycaster = new THREE.Raycaster()
function tick() {
  update()
  render()
  step++

  raycaster.setFromCamera(mouse, camera)
  const intersects = raycaster.intersectObjects(scene.children)

  if(intersects.length > 0){

  }

  renderer.render(scene, camera)
  requestAnimationFrame(tick)

  const colorBuffer = <THREE.BufferAttribute>pGeometry.attributes.color;
  const positionBuffer = <THREE.BufferAttribute>pGeometry.attributes.position;
  colorBuffer.needsUpdate = true;
  positionBuffer.needsUpdate = true;
}

function update() {
  updateParticle()
  updateCamera()
//  controls.update();
}

function render() {
  renderer.render(scene, camera)
}

function resize() {
  camera.aspect = window.innerWidth / window.innerHeight
  camera.updateProjectionMatrix()

  renderer.setSize(window.innerWidth, window.innerHeight)
}

window.addEventListener("load", init);
window.addEventListener("resize", resize);