import * as React from "react"
import * as moment from "moment";

const Footer = (props: any) => {
  const year = moment().format("YYYY");
  return (
    <footer className="footer">
      <small>
        Copyright ©︎1995-{year} ICA Co., Ltd All rights reserved
      </small>
    </footer>
  );
};

export default Footer;
