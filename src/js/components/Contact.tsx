import * as React from "react"
import { Helmet } from "react-helmet";
import posed from "react-pose";
import { Email } from "../lib/smtp"

const Wrapper = posed.div({
  staggerChildren: 50
})

const Inline = posed.div({
  enter: {y: 0, opacity: 1},
  exit: {y: 0, opacity: 0}
})

const Contact = (props: any) => {
  let canvas: any;
  const initialState = { value: "", error: "" };
  const [name, setName] = React.useState(initialState);
  const [tel, setTel] = React.useState({value: "", error: ""});
  const [email, setEmail] = React.useState(initialState);
  const [inquiry, setInquiry] = React.useState(initialState);
  const [disabled, setDisabled] = React.useState(true);
  const [fetch ,setFetch] = React.useState(false);


  const errorCheck = () => {
    if(name.value === "" || email.value === "" || inquiry.value === ""){
      setDisabled(true);
    }else if(name.error !== "" && email.error !== "" && inquiry.error !== ""){
      setDisabled(true);
    }else{
      setDisabled(false);
    }    
  }

  const handleChange = (ev: any) => {
    const target = ev.target.name;
    switch(target){
      case "name":
        setName({...name, value: ev.target.value});
        break;
      case "tel":
        setTel({...tel, value: ev.target.value});
        break;
      case "email":
        setEmail({...email, value: ev.target.value});
        break;
      case "inquiry":
        setInquiry({...inquiry, value: ev.target.value});
        break;
    }
  }

  const handleBlur = (ev: any) => {
    const target = ev.target.name;
    switch(target){
      case "name":
        if(ev.target.validationMessage){
          setName({...name, error: ev.target.validationMessage});
        }else{
          setName({...name, error: ""});
        }
        break;
      case "tel":
        if(ev.target.validationMessage){
          setTel({...tel, error: ev.target.validationMessage});
        }else{
          setTel({...tel, error: ""});
        }
        break;
      case "email":
        if(ev.target.validationMessage){
          setEmail({...email, error: ev.target.validationMessage});
        }else{
          setEmail({...email, error: ""});
        }
        break;
      case "inquiry":
        if(ev.target.validationMessage){
          setInquiry({...inquiry, error: ev.target.validationMessage});
        }else{
          setInquiry({...inquiry, error: ""});
        }
        break;
    }
  }

  React.useEffect(() => {
    errorCheck();
  }, [name, email, inquiry])

  const handleReset = () => {
    setName(initialState);
    setTel(initialState);
    setEmail(initialState);
    setInquiry(initialState);    
  }

  const handleFormSubmit = (ev: any) => {
    ev.preventDefault();
    const dialog = confirm("こちらでよろしいですか？");
    if( disabled === true || dialog === false ) return false;
      Email.send({
        //  SecureToken: "11de87bd-704b-4b3b-941d-93fde06a682d",
        SecureToken: "d2b5cee7-0829-4631-ae96-3e53aeb982e2",
        // Host : "smtp.gmail.com",
        // Username : process.env.FORM_USER,
        // Password : process.env.FORM_PASS,
        To : "web@ica.jp",
        // To : "ikeda@ica.jp",
        From : "noreply@ica.jp",
        Subject : "お問い合わせがありました",
        Body : `名前： ${name.value} <br />TEL： ${tel.value} <br />メールアドレス： ${email.value}<br />内容： ${inquiry.value}`
      }).then((message) => {
        console.log(message)
        setFetch(false);
        props.history.push("/thanks")
      });    
  }

  const errmsg = (err: any) => {
    return(
      <span className="errmsg">
        {err}
      </span>
    )
  }

  return (
    <Wrapper className="ce">
      <div className={ fetch ? "overlay show" : "overlay" }>
        <div className="sk-folding-cube">
          <div className="sk-cube1 sk-cube" />
          <div className="sk-cube2 sk-cube" />
          <div className="sk-cube4 sk-cube" />
          <div className="sk-cube3 sk-cube" />
        </div>        
      </div>
      <Helmet>
        <meta charSet="utf-8" />
        <title>株式会社ＩＣＡ</title>
        <link rel="icon" href="favicon.ico"/>
      </Helmet>
      <section className="contents contact">
        <h2 className="page_title">
          Contact
        </h2>
        <p className="page_title_low">
          お気軽にお問い合わせください。
        </p>
        <Inline>
          <div className="inline">
            <form action="#" onBlur={handleBlur}>
              <div className="flex form_wrapper">
                <div className="fl">
                  <label htmlFor="">
                    <span>Name *:</span>
                    <input type="text" name="name" className={name.error !== "" ? "err" : ""} value={name.value} onChange={handleChange} required />
                    { errmsg(name.error) }
                  </label>
                  <label htmlFor="">
                    <span>TEL:</span>
                    <input type="tel" name="tel" className={tel.error !== "" ? "err" : ""} value={tel.value} onChange={handleChange} />
                    { errmsg(tel.error) }
                  </label>
                  <label htmlFor="">
                    <span>E-mail Address *:</span>
                    <input type="email" name="email" className={email.error !== "" ? "err" : ""} value={email.value} onChange={handleChange} required />
                    { errmsg(email.error) }
                  </label>
                </div>
                <div className="fr">
                  <label htmlFor="">
                    <span>Inquiry *:</span>
                    <textarea name="inquiry" className={inquiry.error !== "" ? "err" : ""} value={inquiry.value} onChange={handleChange} required />
                    { errmsg(inquiry.error) }
                  </label>
                </div>
              </div>

              <div className="flex btn_wrapper">
                <input type="reset" value="Reset" onClick={handleReset} />
                <input type="button" value="Submit" onClick={handleFormSubmit} disabled={disabled ? true : false } />
              </div>
            </form>
          </div>
        </Inline>
      </section>
    </Wrapper>
  );
};

export default Contact;
