import * as React from "react"
import { Helmet } from "react-helmet";
import posed from "react-pose";
import { Pagetop } from "./Pagetop";

const Wrapper = posed.div({
  staggerChildren: 50
})

const Inline = posed.div({
  enter: {y: 0, opacity: 1},
  exit: {y: 0, opacity: 0}
})

const Topics = (props: any) => {
  const [posts, setPosts] = React.useState<any>([]);
  const article = React.useRef(null);
  const contents = React.useRef(null);

  React.useEffect(() => {
    fetch("/js/topics.json")
      .then((response) => {
        return response.json();
      })
      .then((resJson) => {
        let json = resJson
        for(let i = 0, len = json.length; i < len; i++){
          json[i].isShow = false
        };
        setPosts(json)
      });
  }, [])

  const post = posts.map((post: any, index: string) => {
    let btnText = "Read"
    const createMarkup = (text: string) => {
      return {__html: text};
    }

    const toggleText = (ev: any) => {
      const txt = ev.target.previousElementSibling;
      txt.classList.toggle("active");
    }

    return(
      <li key={post.key}>
        <span className="date">{post.date}</span>
        <span className="title">{post.title}</span>
        <p className="txt active" dangerouslySetInnerHTML={createMarkup(post.text)} />
        {/* <span className="sp_txtbtn" onClick={toggleText}>
          {btnText}
        </span> */}
      </li>
    )
  });

  return (
    <Wrapper>
      <Helmet>
        <meta charSet="utf-8" />
        <title>株式会社ＩＣＡ</title>
        <link rel="icon" href="favicon.ico"/>
      </Helmet>
      <Pagetop target_pc={article} target_sp={contents} />
      <section className="contents topics" ref={contents} >
        <h2 className="page_title">
          Topics
        </h2>
        <Inline>
          <div className="inline">
            <article ref={article}>
              <ul className="list_tp">
                {posts.length > 0 ? post : null}
              </ul>
            </article>
          </div>
        </Inline>
      </section>
    </Wrapper>
  );
};

export default Topics;
