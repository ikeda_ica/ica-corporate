import * as React from "react";

const err_404 = (props: any) => {
  return (
    <section className="contents page_404">
      <p>
        404 Page not found.
      </p>
    </section>
  );
};

export default err_404;
