import * as React from "react"
import { Helmet } from "react-helmet";
import posed from "react-pose";
import * as SmoothScroll from "smooth-scroll";

const Pagetop_in = posed.div({
  enter: {opacity: 1},
  exit: {opacity: 0}
})

const easingEaseOutCubic = function (currentTime: any, startValue: any, changeValue: any, duration: any) {
  currentTime /= duration;
  currentTime--;
  return changeValue * (currentTime * currentTime * currentTime + 1) + startValue;
};

const scrollToTop = (target: any) => {
  let
    time = 0,
    currentScrollTop = target.scrollTop,
    targetScrollTop = 0,
    scrollInterval = () => {
      if ( target.scrollTop > targetScrollTop ) {
        time += 0.1;
        let ease = easingEaseOutCubic(time, currentScrollTop, -currentScrollTop, 3) ;
        target.scrollTop = ease;
        window.requestAnimationFrame(scrollInterval);
      }
    };
  scrollInterval();
}

export const Pagetop = (props: any) => {
  const handlePageTop = (ev: any) => {
    const elm = window.innerWidth < 1024 ? props.target_sp.current : props.target_pc.current;
    const current = elm.scrollTop;
    scrollToTop(elm);
  }

  return (
    <Pagetop_in className="pagetop">
      <span onClick={handlePageTop}>
        <svg version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 46.001 46.001">
          <g>
            <path d="M5.906,34.998c-1.352,1.338-3.541,1.338-4.893,0c-1.35-1.338-1.352-3.506,0-4.846l19.54-19.148
              c1.352-1.338,3.543-1.338,4.895,0l19.539,19.148c1.352,1.34,1.352,3.506,0,4.846c-1.352,1.338-3.541,1.338-4.893,0L23,19.295
              L5.906,34.998z"/>
          </g>
        </svg>
      </span>
    </Pagetop_in>
  );
};

export default Pagetop;
