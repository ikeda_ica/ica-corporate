import * as React from "react"
import { Helmet } from "react-helmet";
import posed from "react-pose";
import { Link } from "react-router-dom";
import { Pagetop } from "./Pagetop";

const Wrapper = posed.div({
  staggerChildren: 50
})

const Inline = posed.div({
  enter: {y: 0, opacity: 1},
  exit: {y: 0, opacity: 0}
})

const Company = (props: any) => {
  const company = React.useRef(null);
  const contents = React.useRef(null);

  return (
    <Wrapper>
      <Helmet>
        <meta charSet="utf-8" />
        <title>株式会社ＩＣＡ</title>
        <link rel="icon" href="favicon.ico"/>
      </Helmet>
      <Pagetop target_pc={company} target_sp={contents} />
      <section className="contents company" ref={contents}>
        <h2 className="page_title">
          Company
        </h2>
        <Inline>
          <div className="inline">
            <div className="in" ref={company}>
              <div className="fullimage">
                <img src="./assets/img/company_image.jpg" alt=""/>
              </div>
              <div className="detail_wrap">
                <ul className="detail">
                  <li>
                    <span className="label">社名</span>
                    株式会社ＩＣＡ
                  </li>
                  <li>
                    <span className="label">代表取締役</span>
                    青木 之於 （YUKIO AOKI）
                  </li>
                  <li>
                    <span className="label">所在地</span>
                    〒171-0022　東京都豊島区南池袋1-16-15 ダイヤゲート池袋8F
                  </li>
                  <li>
                    <span className="label">電話</span>
                    03-5953-6888
                  </li>
                  <li>
                    <span className="label">FAX</span>
                    03-5956-8425
                  </li>
                  <li>
                    <span className="label">URL</span>
                    https://www.ica.jp
                  </li>
                  <li>
                    <span className="label">会社設立年月日</span>
                    1995年12月1日
                  </li>
                  <li>
                    <span className="label">資本金</span>
                    10,000,000円
                  </li>
                  <li>
                    <span className="label">経営理念</span>
                    誠実・スピード・クオリティ・丁寧な仕事
                  </li>
                  <li>
                    <span className="label">業務内容</span>
                    Webサイト・Webコンテンツ企画、制作、運営管理 <br />
                    モバイル・スマートフォンサイト制作、運営管理 <br />
                    デジタルサイネージコンテンツ制作、運営管理 <br />
                    個人情報保護体制構築支援及びコンサルティング
                    各種印刷物企画、制作
                  </li>
                  <li>
                    <span className="label">関連会社</span>
                    株式会社 SITE-DESIGNS
                  </li>
                  <li>
                    <span className="label">顧問弁護士</span>
                    早川　忠孝
                  </li>
                  <li>
                    <span className="label">顧問税理士</span>
                    タップ総合会計鑑定事務所（土屋　康成・小島　諭）
                  </li>
                  <li>
                    <span className="label">顧問社会保険労務士</span>
                    山田　良之                  
                  </li>
                  <li>
                    <span className="label">主な取引先</span>
                    株式会社 I&S BBDO <br />
                    株式会社 池袋ショッピングパーク <br />
                    伊藤忠インタラクティブ 株式会社 <br />
                    伊藤忠食品 株式会社 <br />
                    情報技術開発 株式会社（tdi）<br />
                    スポーツクラブNAS 株式会社 <br />
                    株式会社 セブンカルチャーネットワーク  <br />
                    株式会社 そごう・西武 <br />
                    株式会社 たち吉 <br />
                    株式会社 ツムラ <br />
                    凸版印刷 株式会社 <br />
                    株式会社 八ヶ岳高原ロッジ <br />
                    ほか （50音順）                
                  </li>
                  <li>
                    <span className="label">主なWebサイト制作実績</span>
                    株式会社　そごう・西武 <br />
                    株式会社　たち吉 <br />
                    株式会社　八ヶ岳高原ロッジ <br />
                    株式会社　池袋ショッピングパーク <br />
                    株式会社　ツムラ <br />
                    スポーツクラブNAS株式会社 <br />
                    ほか
                  </li>
                  <li>
                    <span className="label">情報セキュリティ</span>
                    ISMS（ISO27001）を2014年12月取得<br />
                    <Link to="/policy">当社のプライバシーポリシーについて</Link>
                  </li>
                </ul>
                <div className="gmap">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1629.8108426796239!2d139.71013629245314!3d35.726811604905016!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xfd6d746ecd990208!2z77yI5qCq77yJ77yp77yj77yh!5e0!3m2!1sja!2sjp!4v1563930491222!5m2!1sja!2sjp" />
                </div>
              </div>
            </div>
          </div>
        </Inline>
      </section>
    </Wrapper>
  );
};

export default Company;
