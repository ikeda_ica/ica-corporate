import * as React from "react"
import { Helmet } from "react-helmet";
import posed from "react-pose";

import Slider from "react-slick";
import "slick-carousel/slick/slick-theme.css";
import "slick-carousel/slick/slick.css";

const Wrapper = posed.div({
  staggerChildren: 50
})

const Inline = posed.div({
  enter: {opacity: 1},
  exit: {opacity: 0}
})

const Thanks = (props: any) => {
  const gal_list: Array<string> = [
    "./assets/img/gallery/DSC03611.jpg",
    "./assets/img/gallery/DSC03612.jpg",
    "./assets/img/gallery/DSC03613.jpg",
    "./assets/img/gallery/DSC03615.jpg",
    "./assets/img/gallery/DSC03616.jpg",
    "./assets/img/gallery/DSC03617.jpg"
  ]

  const [sliderMainRef, setSliderMainRef] = React.useState();
  const [sliderSubRef, setSliderSubRef] = React.useState();
  const [respond, setRespond] = React.useState(true);

  let timer: any = null;
  const handleResize = () => {
    const w = window.innerWidth;
    if(w > 1024){
      setRespond(true);
    }else{
      setRespond(false);
    }    
  }
  React.useEffect(() => {
    window.addEventListener("load", ev => {
      window.setTimeout(() => {
        console.log("gallery loaded")
        handleResize();
      }, 1000 );
    })
    window.addEventListener("resize", ev => {
      clearTimeout( timer );
      timer = setTimeout(() => {
        handleResize();
      }, 300 );
    });
  }, []);

  return (
    <Wrapper>
      <Helmet>
        <meta charSet="utf-8" />
        <title>株式会社ＩＣＡ</title>
        <link rel="icon" href="favicon.ico"/>
      </Helmet>
      <section className="contents gallery">
        <h2 className="page_title">
          Gallery
        </h2>
        <Inline>
          <div className="inline">
            <div className="in">
              <div className="gal_wrapper">
                <div className="gal">
                  <Slider
                    dots={false}
                    infinite={true}
                    speed={500}
                    centerPadding={"0px"}
                    slidesToShow={1}
                    slidesToScroll={1}
                    asNavFor={sliderSubRef}
                    vertical={respond}
                    focusOnSelect={true}
                    ref={ elm => elm !== null ? setSliderMainRef(elm) : null}
                  >
                    <div>
                      <img src="./assets/img/gallery/_67A9546.JPG" alt="" className="gal_main" />
                    </div>
                    <div>
                      <img src="./assets/img/gallery/_67A9591.JPG" alt="" className="gal_main" />
                    </div>
                    <div>
                      <img src="./assets/img/gallery/_67A9626.JPG" alt="" className="gal_main" />
                    </div>
                    <div>
                      <img src="./assets/img/gallery/DSC03644.jpg" alt="" className="gal_main" />
                    </div>
                    <div>
                      <img src="./assets/img/gallery/DSC03690.jpg" alt="" className="gal_main" />
                    </div>
                  </Slider>
                </div>
                <div className="gal_pagers">
                  <Slider
                    speed={500}
                    infinite={true}
                    slidesToShow={3}
                    slidesToScroll={1}
                    asNavFor={sliderMainRef}
                    centerPadding={"0px"}
                    vertical={respond}
                    verticalSwiping={respond}
                    centerMode={true}
                    focusOnSelect={true}
                    ref={ elm => elm !== null ? setSliderSubRef(elm) : null}
                  >
                    <div>
                      <img src="./assets/img/gallery/_67A9546.JPG" alt="" className="gal_pager" />
                    </div>
                    <div>
                      <img src="./assets/img/gallery/_67A9591.JPG" alt="" className="gal_pager" />
                    </div>
                    <div>
                      <img src="./assets/img/gallery/_67A9626.JPG" alt="" className="gal_pager" />
                    </div>
                    <div>
                      <img src="./assets/img/gallery/DSC03644.jpg" alt="" className="gal_pager" />
                    </div>
                    <div>
                      <img src="./assets/img/gallery/DSC03690.jpg" alt="" className="gal_pager" />
                    </div>
                  </Slider>
                </div>
              </div>
            </div>
          </div>
        </Inline>
      </section>
    </Wrapper>
  );
};

export default Thanks;
