import * as React from "react"
import { Link } from "react-router-dom";

const Header = (props: any) => {
  const [active, setActive] = React.useState("");
  const [menu, setMenu] = React.useState(false);

  React.useEffect(() => {
    setActive(props.active)
  });

  const handleMenu = () => {
    setMenu(!menu);
  }

  return (
    <header className="header">
      <h1 className="logo">
        <Link to="/">
          { props.darkmode === true ?
            <img src="./assets/img/logo_w.svg" alt=""/>
            :
            <img src="./assets/img/logo_b.svg" alt=""/>
          }
        </Link>
      </h1>
      <div className={ menu ? "sp_bar active" : "sp_bar"}>
        <a href="#" className="sp_bar_btn" onClick={handleMenu}>
          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="21" viewBox="0 0 24 21">
            <path id="bars-solid" d="M.857,63.857H23.143A.857.857,0,0,0,24,63V60.857A.857.857,0,0,0,23.143,60H.857A.857.857,0,0,0,0,60.857V63A.857.857,0,0,0,.857,63.857Zm0,8.571H23.143A.857.857,0,0,0,24,71.571V69.429a.857.857,0,0,0-.857-.857H.857A.857.857,0,0,0,0,69.429v2.143A.857.857,0,0,0,.857,72.429ZM.857,81H23.143A.857.857,0,0,0,24,80.143V78a.857.857,0,0,0-.857-.857H.857A.857.857,0,0,0,0,78v2.143A.857.857,0,0,0,.857,81Z" transform="translate(0 -60)"/>
          </svg>
        </a>
        <ul className="sp_nav">
          <li className={ active === "/" ? "active" : ""}>
            <Link to="/" onClick={handleMenu}>Index</Link>
          </li>
          {/* <li className={ active === "/about" ? "active" : ""}>
            <Link to="/about">About us</Link>
          </li> */}
          <li className={ active === "/company" ? "active" : ""}>
            <Link to="/company" onClick={handleMenu}>Company</Link>
          </li>
          <li className={ active === "/gallery" ? "active" : ""}>
            <Link to="/gallery" onClick={handleMenu}>Gallery</Link>
          </li>
          <li className={ active === "/topics" ? "active" : ""}>
            <Link to="/topics" onClick={handleMenu}>Topics</Link>
          </li>
          <li className={ active === "/contact" ? "active" : ""}>
            <Link to="/contact" onClick={handleMenu}>Contact</Link>
          </li>
        </ul>
      </div>
    </header>
  );
};

export default Header;
