import * as React from "react"
import { Helmet } from "react-helmet";
import posed from "react-pose";

const Wrapper = posed.div({
  staggerChildren: 50
})

const Inline = posed.div({
  enter: {opacity: 1},
  exit: {opacity: 0}
})

const Thanks = (props: any) => {
  return (
    <Wrapper>
      <Helmet>
        <meta charSet="utf-8" />
        <title>株式会社ＩＣＡ</title>
        <link rel="icon" href="favicon.ico"/>
      </Helmet>
      <section className="contents thanks">
        <Inline>
          <div className="inline_wrapper">
            <p className="txt_done">
              送信完了
            </p>
            <p className="txt">
              このたびはお問い合わせいただき、ありがとうございます。<br />
              後ほど担当者よりご連絡させていただきます。
            </p>
          </div>
        </Inline>
      </section>
    </Wrapper>
  );
};

export default Thanks;
