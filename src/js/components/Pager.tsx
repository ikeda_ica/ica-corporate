import * as React from "react"
import { Link } from "react-router-dom";

const Pager = (props: any) => {
  const [active, setActive] = React.useState("");

  React.useEffect(() => {
    setActive(props.active)
  });

  return (
    <nav className="pager">
      <ul>
        <li className={ active === "/" ? "active" : ""}>
          <Link to="/"><span>Index</span></Link>
        </li>
        {/* <li className={ active === "/about" ? "active" : ""}>
          <Link to="/about"><span>About us</Link>
        </li> */}
        <li className={ active === "/company" ? "active" : ""}>
          <Link to="company"><span>Company</span></Link>
        </li>
        <li className={ active === "/gallery" ? "active" : ""}>
          <Link to="gallery"><span>Gallery</span></Link>
        </li>
        <li className={ active === "/topics" ? "active" : ""}>
          <Link to="topics"><span>Topics</span></Link>
        </li>
        <li className={ active === "/contact" ? "active" : ""}>
          <Link to="contact"><span>Contact</span></Link>
        </li>
      </ul>
    </nav>
  );
};

export default Pager;
