import * as React from "react"
import { Helmet } from "react-helmet";
import posed from "react-pose";

const Wrapper = posed.div({
  staggerChildren: 50
})

const Text = posed.p({
  enter: {y: 0, opacity: 1},
  exit: {y: "-100%", opacity: 0}
})

const About = (props: any) => {
  return (
    <Wrapper>
      <Helmet>
        <meta charSet="utf-8" />
        <title>My Title</title>
        <link rel="icon" href="favicon.ico"/>
      </Helmet>
      <section className="contents company">
        <Text>
          Here is About
        </Text>
      </section>
    </Wrapper>
  );
};

export default About;
