import * as React from "react"
import { Helmet } from "react-helmet";
import posed from "react-pose";
import { Pagetop } from "./Pagetop";

const Wrapper = posed.div({
  staggerChildren: 50
})

const Inline = posed.div({
  enter: {opacity: 1},
  exit: {opacity: 0}
})

const Policy = (props: any) => {
  const policy = React.useRef(null);
  const contents = React.useRef(null);

  return (
    <Wrapper>
      <Helmet>
        <meta charSet="utf-8" />
        <title>株式会社ＩＣＡ</title>
        <link rel="icon" href="favicon.ico"/>
      </Helmet>
      <Pagetop target_pc={policy} target_sp={contents} />
      <section className="contents policy" ref={contents}>
        <h2 className="page_title">
          Policy
        </h2>
        <Inline>
          <div className="inline policy_inline">
            <div className="in" ref={policy}>
              <p>
                <strong>情報セキュリティ基本方針</strong>
                <br />
                当社は、お客さまやお取引先さまからお預かりした情報資産および当社の情報資産を守ることが責務と考え、ここに情報セキュリティ基本方針を定め、実践することを宣言します。
              </p>
              <ul>
                <li>当社は、情報セキュリティ管理体制を確立し、情報資産の適切な管理に努めます。</li>
                <li>当社は、本基本方針に従い社内規程を整備・実施します。</li>
                <li>当社は、情報セキュリティの確保に必要な教育を継続的に行います。 </li>
                <li>当社は、適切な人的・組織的・技術的施策を講じ、情報資産に対する不正な侵入、漏えい、改ざん、紛失・盗難、破壊、利用妨害などが発生しないよう努めます。</li>
                <li>当社は、万一情報資産にセキュリティ上の問題が発生しても、その原因を迅速に究明し、その被害を最小限に止めるとともに再発防止に努めます。 </li>
                <li>当社は、情報セキュリティに関係する法令、国が定める指針、その他の社会的規範を遵守します。</li>
                <li>当社は、以上の活動を継続的に見直し、改善に努めます。</li>
              </ul>
              <p className="author">
                2014年10月1日<br />株式会社　ICA<br />代表取締役社長　青木之於
              </p>
              <p><strong>ISO27001認証取得</strong></p>
              <p>当社は2014年12月2日、ISMS（Information Security Management System情報セキュリティマネジメントシステム)の国際標準規格である、<br />ISO/IEC27001:2014、およびJIS規格であるJIS Q 27001:2014の認証を取得しました。 </p>
              <p>当社は情報セキュリティに対して全社員の意識付けを重要視し、情報セキュリティの強化に日々取り組んでおります。 </p>
              <p>今後もお客さまから一層の信頼をいただける企業であり続けるために、情報セキュリティマネジメントの運用・改善に取り組んでまいります。</p>
              <table className="ismsdetail" cellPadding="0" cellSpacing="0">
                <tbody>
                <tr>
                  <td className="label">登録組織</td>
                  <td>株式会社ICA</td>
                </tr>
                <tr>
                  <td className="label">適用規格</td>
                  <td>ISO/IEC 27001:2013 / JIS Q 27001:2014</td>
                </tr>
                <tr>
                  <td className="label">登録番号</td>
                  <td>IS 615538</td>
                </tr>
                <tr>
                  <td className="label">登録範囲</td>
                  <td>Webサイトの企画・制作 <br />
                  Webサイトの管理・運営 <br />
                  ECサイトの企画・制作・管理・運営
                  </td>
                </tr>
                <tr>
                  <td className="label">初回認証登録日</td>
                  <td>2014年12月2日</td>
                </tr>
                <tr>
                  <td className="label">発効日</td>
                  <td>2014年12月2日</td>
                </tr>
                <tr>
                  <td className="label">最新更新日</td>
                  <td>2017年11月20日</td>
                </tr>
                <tr>
                  <td className="label">有効期限日</td>
                  <td>2020年12月1日</td>
                </tr>
                <tr>
                  <td className="label">審査機関</td>
                  <td>BSI グループ ジャパン株式会社</td>
                </tr>
                </tbody>
              </table>
              <img src="./assets/img/isms_logo.gif" alt="ISMS" className="isms_auth" />
            </div>
          </div>
        </Inline>
      </section>
    </Wrapper>
  );
};

export default Policy;
