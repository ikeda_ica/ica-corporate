import * as React from "react"
import { Helmet } from "react-helmet";
import posed from "react-pose";

const Wrapper = posed.div({
  staggerChildren: 50
})

const Logo = posed.img({
  enter: {y: 0, opacity: 1},
  exit: {opacity: 0}
})

let canvas: HTMLCanvasElement | null;
const Home = (props: any) => {
  const [isShow, setIsShow] = React.useState(false);

  React.useEffect(() => {
    if(props.location.pathname == "/"){
      setIsShow(true);
    }
  }, props.location)

  return (
    <Wrapper>
      <Helmet>
        <meta charSet="utf-8" />
        <title>株式会社ＩＣＡ</title>
        <link rel="icon" href="favicon.ico"/>
      </Helmet>
      <section className="contents index">
        <Logo src="./assets/img/logo.svg" className="index_logo" />
      </section>
    </Wrapper>
  );
};

export default Home;
