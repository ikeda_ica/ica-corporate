const webpack = require('webpack');
const path = require('path');
require('dotenv').config();
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = {
  entry: [
    '@babel/polyfill',
    'whatwg-fetch',
    './src/js/app.tsx'
  ],
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/
      }, {
        test: /\.scss$/,
        use: [
          {
            loader: 'style-loader'
          }, {
            loader: 'css-loader',
            options: {
              url: false
            }
          }, {
            loader: 'postcss-loader',
            options: {
              sourceMap: true,
              plugins: [require('autoprefixer')({grid: true})]
            }
          }, {
            loader: 'sass-loader'
          }
        ]
      }, {
        test: /\.css$/,
        use: [
          {
            loader: 'style-loader'
          }, {
            loader: 'css-loader',
            options: {
              url: false
            }
          }, {
            loader: 'postcss-loader',
            options: {
              sourceMap: true,
              plugins: [require('autoprefixer')({grid: true})]
            }
          }
        ]
      }
    ]
  },
  devtool: 'source-map',
  output: {
    filename: './js/bundle.js',
    path: path.resolve(__dirname, 'dist')
  },
  resolve: {
    extensions: [ '.tsx', '.ts', '.js' ]
  },
  cache: true,
  optimization: {
    minimize: true,
  },
  devServer: {
    contentBase: path.join(__dirname, './dist/'),
    watchContentBase: true,
    historyApiFallback: true,
    compress: true,
    port: 3000,
    host: '0.0.0.0',
    inline: true
  },
  plugins: [
    new BundleAnalyzerPlugin(), 
    new webpack.DefinePlugin({
      'process.env': {
        'FB_API_KEY': JSON.stringify(process.env.FB_API_KEY),
        'FB_AUTH_DOMAIN': JSON.stringify(process.env.FB_AUTH_DOMAIN),
        'FB_DATABASE_URL': JSON.stringify(process.env.FB_DATABASE_URL),
        'FB_PROJECT_ID': JSON.stringify(process.env.FB_PROJECT_ID),
        'FB_STORAGE_BUCKET': JSON.stringify(process.env.FB_STORAGE_BUCKET),
        'FB_MESSAGING_SENDER_ID': JSON.stringify(process.env.FB_MESSAGING_SENDER_ID),
        'GA_ID': JSON.stringify(process.env.GA_ID),
        'NODE_ENV': JSON.stringify('production')
      }
    }),
  ]
};
